<img src="Assets/yahoo_finance_logo.png" alt="drawing" width="200" />

<h1 style="text-align: center;">y-stock-scraper</h1>

<br/>

**y-stock-scraper** is a command line tool which downloads all historical stock data in both `csv` and `json` formats from **[Yahoo Finance](https://in.finance.yahoo.com/)**. This is for educational and reasearch purposes only.

### Supported Sites

Currently supports **[Yahoo Finance](https://in.finance.yahoo.com/)** only.

### Configuring download list

It currently just downloads all stock data for all over the world from [Yahoo Finance](https://in.finance.yahoo.com/).

## Python Support

Supports python >= 3.5
